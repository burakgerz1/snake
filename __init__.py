from st3m import InputState
from st3m.application import Application, ApplicationContext
from ctx import Context
import random

h_block = w_block = 10
screen_coord_left = -90
screen_coord_right = 90
screen_coord_top = -80
screen_coord_bot = 80 
game_hardness_ms = 200

class Snake:
    def __init__(self, x_min, x_max, y_min, y_max):
        self._len_start = 1
        self._len_tot = 1
        self._head_x = 0
        self._head_y = 0
        self._body_x = []
        self._body_y = []
        self._x = 0
        self._y = 0

    def draw(self, ctx: Context):
        ctx.rgb(255, 255, 255).rectangle(self._head_x, self._head_y, h_block, w_block).fill()
        for count, value in enumerate(self._body_x):
            ctx.rgb(255, 255, 255).rectangle(value, self._body_y[count], h_block, w_block).fill()

    def updateHead(self, x_change, y_change):
        self._head_x += x_change
        self._head_y += y_change

    def updateBody(self):
        self._body_x.append(self._head_x)
        self._body_y.append(self._head_y)

    def updateMovement(self):
        if len(self._body_x) > self._len_tot:
            del self._body_y[0]
            del self._body_x[0]

    def bitesItself(self):
        if self._len_tot != self._len_start:
            for count, bodyx in enumerate(self._body_x[:-1]):
                if bodyx == self._head_x:
                    if self._body_y[count] == self._head_y:
                        return True

    def grow(self):
        self._len_tot += 1

class Apple:
    _x = 0
    _y = 0
    def __init__(self, x_min, x_max, y_min, y_max):
        self._x = -20
        self._y = -20

    def draw(self, ctx: Context):
        ctx.rgb(0, 255, 0).rectangle(self._x, self._y, h_block, w_block).fill()

class App(Application):
    _highscore = 0
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._is_running = False
        self._is_game_over = False
        self._snake_x_change = 0
        self._snake_y_change = 0
        self._delta_ms_sum = 0
        self._score = 0
        self._msecs_to_restart = 0
        self._snake = Snake(0, 0, 0, 0)
        self._apple = Apple(0, 0, 0, 0)

    def draw(self, ctx: Context) -> None:
        #Playground
        ctx.rgb(255, 255, 255).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(0, 0, 0).rectangle(screen_coord_left, screen_coord_top, screen_coord_right - screen_coord_left, screen_coord_bot - screen_coord_top).fill()

        #Score
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 16
        ctx.rgb(0, 0, 0)
        ctx.move_to(-20, -100)
        ctx.save()
        ctx.text("Score: " + str(self._score))
        ctx.move_to(-20, -86)
        ctx.text("HighScore: " + str(self._highscore))
        ctx.restore()

        #GameOver
        if self._is_game_over:
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font_size = 30
            ctx.rgb(255, 0, 0)
            ctx.move_to(0, 0)
            ctx.save()
            ctx.text("GAME OVER")
            ctx.restore()

        #Snake and Apple
        self._snake.draw(ctx)
        self._apple.draw(ctx)

    def think(self, ins: InputState, delta_ms: int) -> None:
        if not self._is_game_over:
            self.parseInput(ins)

            if not self.renderOrSkip(delta_ms):
                return

            self._score = self._snake._len_tot -1
            if self._score > int(self._highscore):
                self._highscore = self._score 

            self._snake.updateHead(self._snake_x_change, self._snake_y_change)
            self.snakeTeleportIfWallHit()
            self._snake.updateBody()
            
            if(self._snake.bitesItself()):
                self._is_game_over = True
                return
            
            self._snake.updateMovement()
            
            if self.snakeEatsApple():
                self._apple._x = round(random.randint(screen_coord_left, screen_coord_right- w_block) /w_block) * w_block 
                self._apple._y = round(random.randint(screen_coord_top, screen_coord_bot - w_block) /w_block) * w_block
                self._snake.grow()

        elif self._is_game_over:
            self._msecs_to_restart += delta_ms
            if self._msecs_to_restart >= 2000:
                self.__init__(Application)

    def parseInput(self, ins):
        if ins.captouch.petals[0].pressed:
            self._snake_x_change = 0
            self._snake_y_change = -w_block
        elif ins.captouch.petals[8].pressed:
            self._snake_x_change = -w_block
            self._snake_y_change = 0
        elif ins.captouch.petals[4].pressed:
            self._snake_x_change = 0
            self._snake_y_change = w_block
        elif ins.captouch.petals[6].pressed:
            self._snake_x_change = 0
            self._snake_y_change = w_block
        elif ins.captouch.petals[2].pressed:
            self._snake_x_change = w_block
            self._snake_y_change = 0
   
    def renderOrSkip(self, delta_ms):
        self._delta_ms_sum += delta_ms
        if (self._delta_ms_sum > game_hardness_ms):
            self._delta_ms_sum = 0
            return True
        else:
            return False

    def snakeTeleportIfWallHit(self):
        if self._snake._head_x >= screen_coord_right:
            self._snake._head_x = screen_coord_left
        elif self._snake._head_x < screen_coord_left:
            self._snake._head_x = screen_coord_right - w_block
        elif self._snake._head_y < screen_coord_top:
            self._snake._head_y = screen_coord_bot - w_block
        elif self._snake._head_y > screen_coord_bot:
            self._snake._head_y = screen_coord_top 

    def snakeEatsApple(self):
        if abs(self._snake._head_x - self._apple._x) == 0 and \
            abs(self._snake._head_y - self._apple._y) == 0:
            return True
        else:
            return False
